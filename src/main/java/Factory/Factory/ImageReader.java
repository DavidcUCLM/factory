package Factory.Factory;

interface ImageReader {
    DecodedImage getDecodeImage();
}